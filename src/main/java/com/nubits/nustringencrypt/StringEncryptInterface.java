package com.nubits.nustringencrypt;

/**
 * Created by woolly_sammoth on 24/02/15.
 */


public interface StringEncryptInterface {

    /**
     * encrypt *  
     * @param inputStrings String array of strings to be encrypted
     * @param passphrase The passphrase to encrypt the strings with
     * @return String array of encrypted strings
     */
    public String[] encrypt(String[] inputStrings, String passphrase);

    /**
     * decrypt * 
     * @param encryptedStrings String array of encrypted strings
     * @param passphrase The passphrase to decrypt the strings
     * @return String array of decrypted strings
     */
    public String[] decrypt(String[] encryptedStrings, String passphrase);

    /**
     * encrypt (single) * 
     * @param inputString Single String to encrypt
     * @param passphrase Passphrase to encrypt string with
     * @return encrypted string
     */
    public String encrypt(String inputString, String passphrase);

    /**
     * decrypt (single) *
     * @param encryptedString Single encrypted string
     * @param passphrase Passphrase to decrypt string with
     * @return decrypted string
     */
    public String decrypt(String encryptedString, String passphrase);
}
