package com.nubits.nustringencrypt;

import com.lambdaworks.codec.Base64;
import com.lambdaworks.crypto.SCrypt;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.logging.Logger;

/**
 * Created by sammoth on 24/02/15.
 */

public class StringEncrypt implements StringEncryptInterface {

    private static final Logger LOG = Logger.getLogger("Logger");

    @Override
    public String[] decrypt(String[] encryptedStrings, String passphrase) {
        //ArrayLists are easier to manage
        ArrayList<String> list = new ArrayList<>();
        for (String encryptedString: encryptedStrings) {
            //for each String in the input array, decrypt the string
            list.add(decrypt(encryptedString, passphrase));
        }
        //reformat the ArrayList to a string array and return
        String[] out = new String[list.size()];
        return list.toArray(out);
        
    }

    @Override
    public String decrypt(String encryptedString, String passphrase) {
        try {
            //encrypted string is in the form salt:encryptedString
            //split them apart here to get the salt as a byte[]
            String[] parts = encryptedString.split(":");
            byte[] salt = Base64.decode(parts[0].toCharArray());
            //calculate the key using salt and passphrase
            byte[] key = SCrypt.scrypt(passphrase.getBytes("UTF-8"), salt, 16384, 8, 8, 16);
            //set up key as SecretKey and set up AES Cipher
            SecretKey cipherKey = new SecretKeySpec(key, 0, key.length, "AES");
            Cipher aesCipherDecrypt = Cipher.getInstance("AES/ECB/PKCS5Padding");
            aesCipherDecrypt.init(Cipher.DECRYPT_MODE, cipherKey);
            //Decode the Base64 encoded, encrypted string
            byte[] encryptedBytes = Base64.decode(parts[1].toCharArray());
            //decrypt the string
            byte[] cipherDecode = aesCipherDecrypt.doFinal(encryptedBytes);
            String originalString = new String(cipherDecode, "UTF-8");
            //regenerated salt to check for successful decryption
            byte[] checkSalt = getSalt(originalString);
            if (Arrays.equals(checkSalt, salt)) {
                return originalString;
            } else {
                return "The decryption failed. Please check your passphrase";
            }
        } catch (UnsupportedEncodingException | GeneralSecurityException uee) {
            if (uee.toString().equals("javax.crypto.BadPaddingException: Given final block not properly padded")) {
                return "The decryption failed. Please check your passphrase";
            }
            return uee.toString();
        }
    }

    @Override
    public String[] encrypt(String[] inputStrings, String passphrase) {
        //ArrayLists are easier to manage
        ArrayList<String> list = new ArrayList<>();
        for (String inputString: inputStrings) {
            //for each string in the input array, encrypt the string
            list.add(encrypt(inputString, passphrase));
        }
        //reformat the ArrayList to a String array and return
        String[] out = new String[list.size()];
        return list.toArray(out);
    }

    @Override
    public String encrypt(String inputString, String passphrase) {
        try {
            //The salt is the last 8 bytes of the double SHA256 hash of the inputString
            //using this means we can check for successful decryption as the salts will match before and after 
            byte[] salt = getSalt(inputString);
            if (salt == null) return "Unable to calculate salt";
            //generate the key using scrypt
            byte[] key = SCrypt.scrypt(passphrase.getBytes("UTF-8"), salt, 16384, 8, 8, 16);
            //transfer the key to a SecretKey ready for use with the cipher
            SecretKey cipherKey = new SecretKeySpec(key, 0, key.length, "AES");
            //set up the AES cipher
            Cipher aesCipherEncrypt = Cipher.getInstance("AES/ECB/PKCS5Padding");
            aesCipherEncrypt.init(Cipher.ENCRYPT_MODE, cipherKey);
            //reformat the input string to a byte array and encrypt
            byte[] bytes = inputString.getBytes("UTF-8");
            byte[] encryptBytes = aesCipherEncrypt.doFinal(bytes);
            //encode the encrypted bytes and the salt in base64
            char[] encoded = Base64.encode(encryptBytes);
            char[] outSalt = Base64.encode(salt);
            //the 'encrypted string' is the concatenation of the b64 salt and encrypted bytes.
            return new String(outSalt) + ":" + new String(encoded);
        } catch (UnsupportedEncodingException | GeneralSecurityException uee) {
            System.out.println(uee.toString());
            return uee.toString();
        }
    }

    /**
     * Get the last 8 bytes of the double SHA256 hashed inputString* 
     * @param inputString the string to use to generate the salt
     * @return byte[8] or null on failure
     */
    private byte[] getSalt(String inputString) {
        byte[] digest = doubleSHA256(inputString);
        if (digest == null) return null;
        int digestLength = digest.length;
        byte[] salt = new byte[8];
        for (int i = 0; i < 8; i++) {
            salt[i] = digest[digestLength - (8 - i)];
        }
        return salt;
    }

    /**
     * perform a double sha256 hash on the input string*
     * @param inputString the string to double hash
     * @return byte[64] or null on failure
     */
    private byte[] doubleSHA256(String inputString) {
        MessageDigest md = null;
        try {
            md = MessageDigest.getInstance("SHA-256");
            md.update(inputString.getBytes("UTF-8"));
            byte[] digest = md.digest();
            md.update(digest);
        } catch (UnsupportedEncodingException | NoSuchAlgorithmException uee) {
            System.out.println(uee.toString());
        }
        if (md == null) return null;
        return md.digest();
    }

   
    
}
