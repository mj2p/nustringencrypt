import com.nubits.nustringencrypt.StringEncrypt;
import junit.framework.TestCase;
import org.junit.Test;

public class StringEncryptTest extends TestCase {

    StringEncrypt stringEncrypt = new StringEncrypt();

    @Test
    public void testEncrypt() throws Exception {

        //build a test array of strings typical to NuBot options
        String[] testStrings = new String[6];

        testStrings[0] = "d454549290473872df6a88b3673c0943289h7643672e78f67657396988d20987892ab6783dd27897892326f8903h42789326781dd267832e26782f00921890fc";
        testStrings[1] = "FF3456GH7838367AEED1156256CBHH43";
        testStrings[2] = ":f449tEuQ[:#1";
        testStrings[3] = "barry@example.com";
        testStrings[4] = "BMJ2PnJqZjpDvttJMZTEBiCZzsGL9AL5fE";
        testStrings[5] = "SMJ2PX3KoPbxPqe4o873yCLQQNCjeiiJi9";

        //encrypt them
        String[] outputs = stringEncrypt.encrypt(testStrings, "passPhrase");

        //make sure the appear correctly
        assertEquals(outputs[0], "y8DKWnwlO1U=:ZruvvgsMO1WkOEgOfB0YkgbtWLjZAFHrV+yh3cga3W5NCrtreMJhbu84Fbo0AL3FBbzoxGmJW7h1BtNiwXwUs0jBSg9pYFro7obAvGJWeU9Khhk2z7GPOFnLEZHx99efV/Tvgx2gK/Ml6u7TyucPPtAF+Yz0f7RNMIdKVRNgNJh77VOZ0WNwNkbxq+yVKENr");
        assertEquals(outputs[1], "0Oe9m0x7qJk=:QXbZXeV7sqgDOlyfxk8jypK3WfPT1mTlnlacz/gW6F8CDNMnxSOqsJQ0AC+zpo85");
        assertEquals(outputs[2], "dmq7X0mA0J0=:dBadfc0SxClpIhak6KQTwQ==");
        assertEquals(outputs[3], "e995IIqiQiI=:jQaZHtLUNrpzBN48bgVXrf3mHzSYEOV7hHb+ndfr7FA=");
        assertEquals(outputs[4], "jrBN6NDy7oA=:ZhLm5U6SuCcoAKNzR7/5p/XT9zEFRTyrYLX0ooP91XeCRTG5BGifjQusJ+spdmh9");
        assertEquals(outputs[5], "sDa4G1G3mdk=:IkpMi+v+snGUGlbLFbyul4ujY/iYOA7UOklYbO5WT7NA6duCJneCZYD+1a8mcC9u");

    }

    @Test
    public void testDecrypt() throws Exception {

        String[] testStrings = new String[6];

        testStrings[0] = "y8DKWnwlO1U=:ZruvvgsMO1WkOEgOfB0YkgbtWLjZAFHrV+yh3cga3W5NCrtreMJhbu84Fbo0AL3FBbzoxGmJW7h1BtNiwXwUs0jBSg9pYFro7obAvGJWeU9Khhk2z7GPOFnLEZHx99efV/Tvgx2gK/Ml6u7TyucPPtAF+Yz0f7RNMIdKVRNgNJh77VOZ0WNwNkbxq+yVKENr";
        testStrings[1] = "0Oe9m0x7qJk=:QXbZXeV7sqgDOlyfxk8jypK3WfPT1mTlnlacz/gW6F8CDNMnxSOqsJQ0AC+zpo85";
        testStrings[2] = "dmq7X0mA0J0=:dBadfc0SxClpIhak6KQTwQ==";
        testStrings[3] = "e995IIqiQiI=:jQaZHtLUNrpzBN48bgVXrf3mHzSYEOV7hHb+ndfr7FA=";
        testStrings[4] = "jrBN6NDy7oA=:ZhLm5U6SuCcoAKNzR7/5p/XT9zEFRTyrYLX0ooP91XeCRTG5BGifjQusJ+spdmh9";
        testStrings[5] = "sDa4G1G3mdk=:IkpMi+v+snGUGlbLFbyul4ujY/iYOA7UOklYbO5WT7NA6duCJneCZYD+1a8mcC9u";

        String[] outputs = stringEncrypt.decrypt(testStrings, "passPhrase");
        /*for (String output : outputs) {
            System.out.println(output);
        }*/

        assertEquals(outputs[0], "d454549290473872df6a88b3673c0943289h7643672e78f67657396988d20987892ab6783dd27897892326f8903h42789326781dd267832e26782f00921890fc");
        assertEquals(outputs[1], "FF3456GH7838367AEED1156256CBHH43");
        assertEquals(outputs[2], ":f449tEuQ[:#1");
        assertEquals(outputs[3], "barry@example.com");
        assertEquals(outputs[4], "BMJ2PnJqZjpDvttJMZTEBiCZzsGL9AL5fE");
        assertEquals(outputs[5], "SMJ2PX3KoPbxPqe4o873yCLQQNCjeiiJi9");

    }

    @Test
    public void testBadDecryption() throws Exception {
        String testString = "BMJ2PnJqZjpDvttJMZTEBiCZzsGL9AL5fE";
        String encryptedString = stringEncrypt.encrypt(testString, "PassPhrase");
        assertEquals(encryptedString, "jrBN6NDy7oA=:mYVsraaX1pOs36laUFbUZJxsyMCTmsgI6cgL2TYfJJZdnU10EulZ4cfd+fCzsrfX");
        String decryptedString = stringEncrypt.decrypt(encryptedString, "NotThePassPhrase");
        assertEquals(decryptedString, "The decryption failed. Please check your passphrase");
    }
}